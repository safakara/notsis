<?php

class ogrenci{
	
	private $data;
	
	public function add($no, $ad, $soyad, $vize, $final, $devamsizlikDurumu = false, $butunlemeDurumu = false, $HBN = "none", $harf = null) {
		$this->data[] = array(
								'no' => $no, 
								'ad' => $ad, 
								'soyad' => $soyad, 
								'vize' => $vize, 
								'final' => $final, 
								'devamsizlikDurumu' => $devamsizlikDurumu, 
								'butunlemeDurumu' => $butunlemeDurumu, 
								'HBN' => $HBN, 
								'harf' => $harf
							);
	}
 
	public function set($id, $no, $ad, $soyad, $vize, $final, $devamsizlikDurumu = false, $butunlemeDurumu = false, $HBN = "none", $harf = null) {
		if(!isset($this->data[$id])) 
			return false;
		else
		{
			$this->data[$id] = array(
									'no' => $no, 
									'ad' => $ad, 
									'soyad' => $soyad, 
									'vize' => $vize, 
									'final' => $final, 
									'devamsizlikDurumu' => $devamsizlikDurumu, 
									'butunlemeDurumu' => $butunlemeDurumu, 
									'HBN' => $HBN, 
									'harf' => $harf
								);
			return true;
		
		}      
	}
 
	public function delete($id) {
		unset($this->data[$id]);
	}
 
	public function all() {
		return $this->data;
	}
	
}

?>