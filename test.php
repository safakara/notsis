<?php

/**
 * notsis
 * @package	notsis
 * @author askara
 * @features (ogrenci.class)
 *		- id 				: ogrenci sınıfında yer alan nesnenin index değeri
 *	 	- no				: öğrenci numarası, $ad, $soyad, $vize, $final, $devamsizlikDurumu = false, $butunlemeDurumu = false, $HBN = "none", $harf = null
 *		- ad				: öğrenci ad
 *		- soyad				: öğrenci soyad
 *		- vize				: öğrenci vize notu [0-100]
 *		- final				: öğrenci final notu [0-100]
 *		- devamsizlikDurumu	: öğrenci devamsızlıktan kalıyorsa bu parametre true değeri almalı[varsayılan:false]
 *		- butunlemeDurumu	: öğrenci bütünleme sınavına girmemişse parametre true değeri almalı[varsayılan:false]
 *		- HBN				: öğrencinin dönem sonu ham başarı notu [varsayılan: vize*0.4 + final*0.6 | none]
 *		- harf				: hesaplama sonucu öğrenci notu başarı harfi
 * 	
 * saygılar :)
 */

header('Content-Type: text/html; charset=utf-8');
require_once("ogrenci.class.php");
require_once("process.class.php");


$ogrenci = new ogrenci();
$ogrenci->add("2014638999", "Ahmet", "Kara", 45, 75);
$ogrenci->add("2014638888", "Aytunç", "Yalçınkaya", 50, 60);
$ogrenci->add("2014638777", "Yassin", "Kayugwa", 50, 60, true, false);
$ogrenci->add("2014638666", "Batuhan", "Batu", 10, 15, false, true);
$ogrenci->add("2014638555", "Tanıl", "Yıldır", 100, 60, true, true);
$ogrenci->add("2014638444", "Şeyhmus", "Önen", 77, 60);
$ogrenci->add("2014638333", "Ömer Faruk", "Özcan", 90, 80);
$ogrenci->add("2014638222", "Yaşar", "Binici", 100, 100);
$ogrenci->add("2014638111", "Gökhan", "Demir", 80, 90);


$process = new process();
$result = $process->result($ogrenci->all());


var_dump($result);
//var_dump($process->getAritmetikOrtalama($result));
//var_dump($process->getStandartSapma($result));

//print_r($result);
//print_r($process->getAritmetikOrtalama($result));
//print_r($process->getStandartSapma($result));

?>