<?php

class process{
	
	private $standartSapma 		= 0;
	private $aritmetikOrtalama 	= 0;
	private $vizeKatsayi 		= 0.4;
	private $finalKatsayi 		= 0.6;
	private $bagilDegerlendirmeLimiti	= 20;
	private $standartSapmaLimiti		= 8;
	private $katsayi = array(
								"45" => array(-1.2, -0.8, -0.4, 0.667, 1.378, 2.089, 2.8),
								"55" => array(-1.5, -1, -0.5, 0.5, 1.167, 1.833, 2.5),
								"65" => array(-1.8, -1.2, -0.6, 0.333, 0.956, 1.578, 2.2),
								"70" => array(-2, -1.33, -0.67, 0.22, 0.813, 1.407, 2),
								"75" => array(-2.2, -1.46, -0.73, 0.113, 0.676, 1.238, 1.8),
								"80" => array(-2.6, -1.73, -0.86, -0.11, 0.396, 0.898, 1.4),
								"100" => array(-3, -2, -1, -0.33, 0.111, 0.556, 1)
							);
	private $harfler = array("FF", "DD", "DC", "CC", "CB", "BB", "BA", "AA");
	
	
	private $tmpCount 	= 0;
	private $tmpSum 	= 0;
	private $tmpData	= null;
	
	private function aritmetikOrtalamaHesaplama($data)
	{
		foreach($data as $key => $value) 
		{
			if($value["devamsizlikDurumu"])
				$data[$key]["harf"] = "NA";
			else if($value["butunlemeDurumu"])
				$data[$key]["harf"] = "FG";
			else
			{
				$data[$key]["HBN"] = $this->vizeKatsayi * $data[$key]["vize"] + $this->finalKatsayi * $data[$key]["final"];
				
				if($data[$key]["HBN"] < 30)
					$data[$key]["harf"] = "FF";
				else
				{
					$this->tmpCount++;
					$this->tmpSum += $data[$key]["HBN"];
				}
			}
		}
		
		return $data;
	}
	
	public function getAritmetikOrtalama($data)
	{
		$this->tmpData = $this->aritmetikOrtalamaHesaplama($data);
		
		if(!$this->tmpCount)
			return false;
		else
			return $this->tmpSum / $this->tmpCount;
	}
	
	public function getStandartSapma($tmpData, $aritmetikOrtalama = null)
	{
		if(!@$aritmetikOrtalama)
		{
			$aritmetikOrtalama = $this->getAritmetikOrtalama($tmpData);
			$tmpData = $this->tmpData;
		}
		
		$tmpCount = 0;
		$tmpSum = 0;
		
		foreach($tmpData as $key => $value) 
		{
			if(!($value["harf"] == "NA" OR $value["harf"] == "FF" OR $value["harf"] == "FG"))
			{
				$tmpCount++;
				$tmpSum += pow($tmpData[$key]["HBN"] - $aritmetikOrtalama, 2); 
			}
		}
		
		return sqrt($tmpSum / $tmpCount);
	}
	
	public function result($data){
		$aritmetikOrtalama 	= $this->getAritmetikOrtalama($data);
		$standartSapma 		= $this->getStandartSapma($this->tmpData, $aritmetikOrtalama);
		
		
		$tmpData = $this->tmpData;
		if($this->tmpCount < $this->bagilDegerlendirmeLimiti OR $standartSapma < $this->standartSapmaLimiti)
		{
			foreach($tmpData as $key => $value) 
			{
				$HBN = (int)$tmpData[$key]["HBN"];
				switch ($HBN){
					case ($HBN < 50): 
									$tmpData[$key]["harf"] = $this->harfler[0]; 
									break;
					case $HBN < 60: 
									$tmpData[$key]["harf"] = $this->harfler[1]; 
									break;
					case $HBN < 70: 
									$tmpData[$key]["harf"] = $this->harfler[2]; 
									break;
					case $HBN < 75: 
									$tmpData[$key]["harf"] = $this->harfler[3]; 
									break;
					case $HBN < 80: 
									$tmpData[$key]["harf"] = $this->harfler[4];  
									break;		
					case $HBN < 85: 
									$tmpData[$key]["harf"] = $this->harfler[5]; 
									break;
					case $HBN < 90: 
									$tmpData[$key]["harf"] = $this->harfler[6]; 
									break;
					case $HBN <= 100:
									$tmpData[$key]["harf"] = $this->harfler[7];  
				}
			}
		}
		else
		{
			switch ($aritmetikOrtalama){
						case $aritmetikOrtalama < 45: 
														$i = "45"; 
														break;
						case $aritmetikOrtalama < 55: 
														$i = "55"; 
														break;
						case $aritmetikOrtalama < 65: 
														$i = "65"; 
														break;
						case $aritmetikOrtalama < 70: 
														$i = "70"; 
														break;
						case $aritmetikOrtalama < 75: 
														$i = "75"; 
														break;		
						case $aritmetikOrtalama < 80: 
														$i = "80"; 
														break;
					   default: 						$i = "100"; 
					}
			
			foreach($tmpData as $key => $value) 
			{
				
				$HBN = (int)$tmpData[$key]["HBN"];
				$harf = $tmpData[$key]["harf"];
				
				if(!@$harf)
				{
					for($k = 0; $k < count($this->katsayi); $k++)
					{
						if($HBN < $aritmetikOrtalama + ($this->katsayi[$i][$k] * $standartSapma))
						{
							$tmpData[$key]["harf"] = $this->harfler[$k];
							break;
						}
						else if($k + 1 == count($this->katsayi))
							$tmpData[$key]["harf"] = $this->harfler[$k+1];
					}
				}
			}
		}
		
		return $tmpData;
	}
}

?>